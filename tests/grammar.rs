extern crate colored;
extern crate synergy;

use synergy::evaluate;
use synergy::value::Value;
use synergy::l1::expression::L1State;
use synergy::compiler::Literal;
use std::fs::File;
use std::io::prelude::*;
use synergy::print_expression;

mod grammar {
    include!(concat!(env!("OUT_DIR"), "/grammar.rs"));
}

use self::grammar::*;

fn read_source(filename: &str) -> String {
    let mut f = File::open(filename).expect("File not found");
    let mut source = String::new();
    f.read_to_string(&mut source)
        .expect("Something went wrong reading source");
    source
}

#[test]
fn test_boolean() {
    assert!(boolean("true").is_ok());
    assert!(boolean("false").is_ok());

    // Evaluation
    let true_lit = boolean("true").unwrap();
    let false_lit = boolean("false").unwrap();

    let test_true = evaluate(true_lit.clone()).expect_value();
    let test_false = evaluate(false_lit.clone()).expect_value();

    assert_eq!(test_true, Value::Literal(Literal::Bool(true)));
    assert_eq!(test_false, Value::Literal(Literal::Bool(false)));
}

#[test]
fn test_integer() {
    assert!(integer("1234").is_ok());

    let test_small_integer = evaluate(integer("1234").unwrap()).expect_value();

    assert_eq!(test_small_integer, Value::Literal(Literal::Int32(1234)));
}

#[test]
fn test_hex() {
    assert!(hex("0xc93d288c7fb0dc702d2527a2c51d229fedae762b").is_ok());

    let test_hex = evaluate(hex("0x0123456789ABCDEF").unwrap()).expect_value();
    let expected_hex = Literal::Hex("0123456789ABCDEF".to_owned());

    assert_eq!(test_hex, Value::Literal(expected_hex));
}

#[test]
fn test_expression() {
    assert!(expression("100 + 100").is_ok());

    let test_addition = evaluate(expression("100 + 100").unwrap()).expect_value();

    assert_eq!(test_addition, Value::Literal(Literal::Int32(200)));
}

#[test]
fn test_tuple_builder() {
    assert!(tuple_builder("[ t <- 0 ]").is_ok());
    assert!(tuple_builder("[t <- 0, s <- 1]").is_ok());

    let tuple_source_1 = read_source("./isrc/tuple_1.i");

    let p1 = scope(tuple_source_1.as_ref()).unwrap();

    evaluate(p1[0].clone());
}

#[test]
fn test_if() {
    assert!(conditional("if X then Y else Z").is_ok());
}

#[test]
fn test_perturb() {
    assert!(expression("A @ [t <- 0]").is_ok());
    assert!(expression("A @ [t <- #.t]").is_ok());

    let perturb_source_1 = read_source("./isrc/perturb_1.i");
    let perturb_source_2 = read_source("./isrc/perturb_2.i");

    let p1 = scope(perturb_source_1.as_ref()).unwrap();
    evaluate(p1[0].clone());

    let p1 = scope(perturb_source_2.as_ref()).unwrap();
    let mut l1_state = L1State::new();
    let _l1_res = p1[0].clone().transform(&mut l1_state);
    // println!("{}", print_expression(l1_res.clone(), 0));
    evaluate(p1[0].clone());
}

#[test]
fn test_query() {
    assert!(expression("#.t").is_ok());
}

#[test]
fn test_variable_declaration() {
    assert!(function_or_variable_declaration("x = 0").is_ok());
    assert!(function_or_variable_declaration("x = x + y").is_ok());
    assert!(function_or_variable_declaration("A [x <- 0, y <- 0] = 0").is_ok());
}

#[test]
fn test_function_declaration() {
    assert!(function_or_variable_declaration("f.d = 0").is_ok());
    assert!(
        function_or_variable_declaration("fby.t X Y = if #.t <= 0 then X else Y @ [t <- #.t - 1]")
            .is_ok()
    );
}

#[test]
fn test_function_application() {
    assert!(expression("f.d").is_ok());
    assert!(expression("f.x.y").is_ok());
    assert!(expression("f.x.y!z A").is_ok());
    assert!(expression("f A").is_ok());
    assert!(expression_where("fby.t X Y").is_ok());
    assert!(expression("f.d + g A").is_ok());
    assert!(expression("wvr.d (next.d X) (next.d Y)").is_ok());
}

#[test]
fn test_intension() {
    let intension_source_1 = read_source("./isrc/intension_1.i");
    let intension_source_2 = read_source("./isrc/intension_2.i");

    let intension_test_1 = scope(intension_source_1.as_ref()).unwrap();
    let intension_test_2 = scope(intension_source_2.as_ref()).unwrap();

    let intension_test_1_result = evaluate(intension_test_1[0].clone()).expect_value();

    assert_eq!(Value::Literal(Literal::Int32(0)), intension_test_1_result);

    let intension_test_2_result = evaluate(intension_test_2[0].clone()).expect_value();

    assert_eq!(Value::Literal(Literal::Int32(3)), intension_test_2_result);
}

#[test]
fn test_fib_stream() {
    let fib_source = read_source("./isrc/fib_stream.i");

    assert!(scope(fib_source.as_ref()).is_ok());
    let body = scope(fib_source.as_ref()).unwrap();
    evaluate(body[0].clone());
}

#[test]
fn test_fib_function() {
    let fib_function_source = read_source("./isrc/fib_function.i");
    assert!(scope(fib_function_source.as_ref()).is_ok());
    let body = scope(fib_function_source.as_ref()).unwrap();
    let _result = evaluate(body[0].clone());
}

#[test]
fn test_naturals() {
    let naturals_source = read_source("./isrc/naturals.i");
    assert!(scope(naturals_source.as_ref()).is_ok());
    let body = scope(naturals_source.as_ref()).unwrap();
    let mut l1_state = L1State::new();
    let _l1_res = body[0].clone().transform(&mut l1_state);
    // println!("{}", print_expression(l1_res.clone(), 0));
    let _result = evaluate(body[0].clone());
}

#[test]
fn test_wvr() {
    let wvr_source = read_source("./isrc/wvr.i");
    assert!(scope(wvr_source.as_ref()).is_ok());
    let body = scope(wvr_source.as_ref()).unwrap();
    let _result = evaluate(body[0].clone());
}

#[test]
fn test_fib_sum() {
    let fib_sum_source = read_source("./isrc/fib_sum.i");
    assert!(scope(fib_sum_source.as_ref()).is_ok());
    let body = scope(fib_sum_source.as_ref()).unwrap();
    let _result = evaluate(body[0].clone());
}

#[test]
fn test_input_script() {
    let input_script = read_source("./isrc/input_script.i");
    let scope = scope(input_script.as_ref());
    assert!(scope.is_ok());
    let scope = scope.unwrap();
    let mut l1_state = L1State::new();
    let l1_res = scope[0].clone().transform(&mut l1_state);
    println!("{}", print_expression(l1_res.clone(), 0));
}

#[test]
fn test_output_script() {
    let output_script = read_source("./isrc/output_script.i");
    let scope = scope(output_script.as_ref());
    assert!(scope.is_ok());
    let scope = scope.unwrap();
    let mut l1_state = L1State::new();
    let l1_res = scope[0].clone().transform(&mut l1_state);
    println!("{}", print_expression(l1_res.clone(), 0));
}

#[test]
fn test_prelude() {
    let prelude_source = read_source("./isrc/prelude.i");
    assert!(scope(prelude_source.as_ref()).is_ok());
}
