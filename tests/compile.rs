extern crate synergy;
extern crate futures;

use synergy::compiler::{VM, Module, IFrame, Literal};
use synergy::l1::expression::{L1Expression, L1State};
use synergy::expression::Expression;
use synergy::intensional::Intensional;
use synergy::value::Value;
use synergy::context::Context;
use std::fs::File;
use std::io::prelude::*;
use std::error::Error;

mod grammar {
    include!(concat!(env!("OUT_DIR"), "/grammar.rs"));
}

use self::grammar::*;

fn read_source(filename: &str) -> String {
    let mut f = File::open(filename).expect("File not found");
    let mut source = String::new();
    f.read_to_string(&mut source)
        .expect("Failed to read source file");
    source
}

fn transform_l1(l1: L1Expression) -> Expression {
    let mut l1_state = L1State::new();
    l1.transform(&mut l1_state)
}

fn eval(vm: &mut VM, expression: Expression, module: Module) -> Result<Intensional, Box<Error>> {
    let iframe = IFrame::new(expression, module);
    vm.input(iframe);
    let () = vm.process_input().unwrap();
    vm.reverse_output();
    vm.process_output()
}

//------------------------------------------------------------------------------
// Literal Compilation
//------------------------------------------------------------------------------

fn true_value() -> Value {
    Value::Literal(Literal::Bool(true))
}

fn false_value() -> Value {
    Value::Literal(Literal::Bool(false))
}

fn int32_value() -> Value {
    Value::Literal(Literal::Int32(123456789))
}

fn hex_value() -> Value {
    Value::Literal(Literal::Hex("c93d288c7fb0dc702d2527a2c51d229fedae762b".to_owned()))
}

#[test]
fn test_boolean() {
    let mut vm = VM::new();
    let module = Module::new();

    let true_l1 = boolean("true").unwrap();
    let false_l1 = boolean("false").unwrap();
    let true_exp = transform_l1(true_l1);
    let false_exp = transform_l1(false_l1);

    let true_res = eval(&mut vm, true_exp.clone(), module.clone()).unwrap();
    let false_res = eval(&mut vm, false_exp.clone(), module.clone()).unwrap();

    assert_eq!(true_res.expect_value().unwrap(), true_value());
    assert_eq!(false_res.expect_value().unwrap(), false_value());
}

#[test]
fn test_int32() {
    let mut vm = VM::new();
    let module = Module::new();
    let int32_l1 = integer("123456789").unwrap();
    let int32_exp = transform_l1(int32_l1);
    let int32_res = eval(&mut vm, int32_exp.clone(), module.clone()).unwrap();
    assert_eq!(int32_res.expect_value().unwrap(), int32_value());
}

#[test]
fn test_hex() {
    let mut vm = VM::new();
    let module = Module::new();
    let hex_l1 = hex("0xc93d288c7fb0dc702d2527a2c51d229fedae762b").unwrap();
    let hex_exp = transform_l1(hex_l1);
    let hex_res = eval(&mut vm, hex_exp.clone(), module.clone()).unwrap();
    assert_eq!(hex_res.expect_value().unwrap(), hex_value());
}

//------------------------------------------------------------------------------
// Tuples
//------------------------------------------------------------------------------

fn tuple1_value() -> Value {
    use synergy::context::Tuple;
    use synergy::value::Dimension;
    use synergy::value::Value::{Literal, Identifier};
    use synergy::compiler::Literal::Int32;
    Value::Context(
        Context(vec![
            Tuple {
                dim: Dimension { p: vec![0], v: Identifier("f2ebea6a8e".to_string()) },
                ord: Literal(Int32(0))
            },
            Tuple {
                dim: Dimension { p: vec![0], v: Identifier("2802614995".to_string()) },
                ord: Literal(Int32(0))
            }
        ])
    )
}

#[test]
fn test_tuple() {
    let mut vm = VM::new();
    let module = Module::new();
    let tuple1_source = read_source("./isrc/tuple_1.i");
    let tuple1_l1 = scope(tuple1_source.as_ref()).unwrap();
    let tuple1_exp = transform_l1(tuple1_l1[0].clone());
    let tuple1_res = eval(&mut vm, tuple1_exp.clone(), module.clone()).unwrap();
    assert_eq!(tuple1_res.expect_value().unwrap(), tuple1_value());
}

//------------------------------------------------------------------------------
// Identifier
//------------------------------------------------------------------------------

fn id_value() -> Value {
    use synergy::value::Value::Literal;
    use synergy::compiler::Literal::Int32;
    Literal(Int32(1))
}

#[test]
fn test_identifier() {
    let mut vm = VM::new();
    let module = Module::new();
    let id_source = read_source("./isrc/identifier.i");
    let id_l1 = scope(id_source.as_ref()).unwrap();
    let id_exp = transform_l1(id_l1[0].clone());
    let id_res = eval(&mut vm, id_exp.clone(), module.clone()).unwrap();
    assert_eq!(id_res.expect_value().unwrap(), id_value());
}

//------------------------------------------------------------------------------
// Operator
//------------------------------------------------------------------------------

fn operator_test_value() -> Value {
    use synergy::value::Value::Literal;
    use synergy::compiler::Literal::Bool;
    Literal(Bool(true))
}

#[test]
fn test_operator() {
    let mut vm = VM::new();
    let module = Module::new();
    let op_source = read_source("./isrc/operator.i");
    let op_l1 = scope(op_source.as_ref()).unwrap();
    let op_exp = transform_l1(op_l1[0].clone());
    let op_res = eval(&mut vm, op_exp.clone(), module.clone()).unwrap();
    assert_eq!(op_res.expect_value().unwrap(), operator_test_value());
}

//------------------------------------------------------------------------------
// Fib
//------------------------------------------------------------------------------

fn fib_value() -> Value {
    use synergy::compiler::Literal::Int32;    
    Value::Literal(Int32(0))
}

#[test]
fn test_fib_stream() {
    let mut vm = VM::new();
    let module = Module::new();
    let fib_source = read_source("./isrc/fib.i");
    let fib_l1 = scope(fib_source.as_ref()).unwrap();
    let fib_exp = transform_l1(fib_l1[0].clone());
    let fib_res = eval(&mut vm, fib_exp, module.clone()).unwrap();
    assert_eq!(fib_res.expect_value().unwrap(), fib_value());
}
