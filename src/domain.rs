use colored::*;
use std::collections::HashSet;
use std::iter::FromIterator;
use std::fmt;

use value::*;

#[derive(Eq, PartialEq, Clone)]
pub struct Domain(pub HashSet<Dimension>);

impl fmt::Debug for Domain {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut s = "{".bright_white().to_string();
        let d = self.0.clone();
        if d.clone().len() > 0 {
            if d.clone().len() > 1 {
                let mut i = 0;
                for di in d.clone() {
                    if i == 0 {
                        s = format!("{}{:?}, ", s, di);
                        i += 1;
                    } else if i == (d.len() - 1) {
                        s = format!("{}{:?}", s, di);
                        i += 1;
                    } else {
                        s = format!("{}{:?}, ", s, di);
                        i += 1;
                    }
                }
            } else {
                let di: Vec<Dimension> = d.iter().cloned().collect();
                s = format!("{}{:?}", s, di[0].clone());
            }
        }
        write!(f, "{}{}", s, "}".bright_white())
    }
}

impl Domain {
    pub fn new() -> Domain {
        Domain(HashSet::new())
    }

    pub fn from_vec(v: Vec<Dimension>) -> Domain {
        Domain(HashSet::from_iter(v.iter().cloned()))
    }

    pub fn to_vec(&self) -> Vec<Dimension> {
        let mut vec: Vec<Dimension> = vec![];
        vec.extend(self.0.clone().into_iter());
        vec
    }

    pub fn len(&self) -> usize {
        self.0.len()
    }

    pub fn push(&mut self, dim: Dimension) -> bool {
        self.0.insert(dim)
    }

    pub fn contains(&self, dim: Dimension) -> bool {
        self.0.contains(&dim)
    }

    pub fn is_subset(&self, other: Domain) -> bool {
        self.0.is_subset(&other.0)
    }

    pub fn union(&self, other: Domain) -> Domain {
        let u = self.0.union(&other.0).cloned().collect();
        Domain(u)
    }

    pub fn merge(&self, other: Vec<Dimension>) -> Domain {
        let other = HashSet::from_iter(other.iter().cloned());
        let u = self.0.union(&other).cloned().collect();
        Domain(u)
    }

    pub fn difference(&self, other: Domain) -> Domain {
        let d = self.0.difference(&other.0).cloned().collect();
        Domain(d)
    }

}
