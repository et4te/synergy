use environment::{Environment, ExternalEnvironment};
use std::rc::Rc;

#[derive(Clone)]
pub struct Module {
    pub environment: Rc<Environment>,
    pub external_environment: Rc<ExternalEnvironment>,
}

impl Module {
    pub fn new() -> Module {
        Module {
            environment: Rc::new(Environment::new()),
            external_environment: Rc::new(ExternalEnvironment::new()),
        }
    }

    pub fn merge_environment(&mut self, rhs: Environment) {
        let environment = Rc::make_mut(&mut self.environment);
        environment.merge(rhs.clone())
    }
}

