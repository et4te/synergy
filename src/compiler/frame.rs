use expression::Expression;
use context::{Tuple, Context};
use domain::Domain;
use compiler::{Module, Literal};
use cache::ICache;
use intensional::Intensional;
use value::Value;
use either::Either;
use print_expression;
use std::error::Error;
use std::rc::Rc;
use std::collections::HashSet;
use std::fmt;

type Identifier = String;

fn apply_operator(id: Identifier, params: Vec<Value>) -> Result<Intensional, Box<Error>> {
    match id.as_ref() {
        "*" => {
            let a = params[0].expect_integer();
            let b = params[1].expect_integer();
            Ok(Intensional::Value(Value::Literal(Literal::Int32(a * b))))
        }
        "+" => {
            let paramsi: Vec<u32> = params.iter().map(|p| p.expect_integer()).collect();
            Ok(Intensional::Value(Value::Literal(Literal::Int32(
                paramsi.iter().fold(0, |sum, n| sum + n)
            ))))
        }
        "-" => {
            let a = params[0].expect_integer();
            let b = params[1].expect_integer();
            Ok(Intensional::Value(Value::Literal(Literal::Int32(a - b))))
        }
        "<=" => {
            let a = params[0].expect_integer();
            let b = params[1].expect_integer();
            Ok(Intensional::Value(Value::Literal(Literal::Bool(a <= b))))
        }
        other =>
            Err(From::from(format!("Unrecognised primitive operator {:?}", other)))
    }
}

#[derive(Clone)]
pub struct VM {
    pub input_stack: Vec<IFrame>,
    pub output_stack: Vec<Either<Intensional, StackOp>>,
    pub result_stack: Vec<Intensional>,
    pub cache: ICache,
}

impl VM {

    pub fn new() -> VM {
        VM {
            input_stack: vec![],
            output_stack: vec![],
            result_stack: vec![],
            cache: ICache::new(),
        }
    }

    pub fn input(&mut self, input: IFrame) {
        self.input_stack.push(input)
    }

    pub fn output(&mut self, output: Either<Intensional, StackOp>) {
        self.output_stack.push(output)
    }

    pub fn result(&mut self, result: Intensional) {
        self.result_stack.push(result)
    }

    pub fn result_i(&mut self, i: usize, result: Intensional) {
        self.result_stack[i] = result
    }

    pub fn result_front(&mut self, result: Intensional) {
        self.result_stack = [vec![result], self.result_stack.clone()].concat();
    }

    pub fn result_pop_front(&mut self) -> Option<Intensional> {
        if self.result_stack.len() > 0 {
            let result = self.result_stack[0].clone();
            self.result_stack = self.result_stack[1..].to_vec();
            Some(result)
        } else {
            None
        }
    }

    pub fn drain_results(&mut self, start: usize, lim: usize) {
        // println!("pre: {:?}", self.result_stack.clone());
        let mut lhs = vec![];
        for i in 0..start {
            lhs.push(self.result_stack[i].clone());
        }
        let mut rhs = vec![];
        for i in start+lim..self.result_stack.len() {
            rhs.push(self.result_stack[i].clone());
        }
        self.result_stack = [lhs, rhs].concat();
        // println!("post: {:?}", self.result_stack.clone());
    }

    pub fn reverse_output(&mut self) {
        self.output_stack.reverse();
    }

    pub fn process_input(&mut self) -> Result<(), Box<Error>> {
        loop {
            match self.input_stack.pop() {
                Some(frame) =>
                    frame.process_input(self).unwrap(),
                None =>
                    return Ok(())
            };
        }
    }

    pub fn process_output(&mut self) -> Result<Intensional, Box<Error>> {
        loop {
            match self.output_stack.pop() {
                Some(output) =>
                    match output {
                        Either::Left(value) =>
                            Ok(self.result_stack.push(value.clone())),
                        Either::Right(stack_op) =>
                            stack_op.process(self),
                    },
                None => {
                    return Ok(self.result_stack.clone().pop().unwrap())
                }
            };
        }
    }

}

#[derive(Clone, Debug)]
pub enum StackOp {
    ProcessInput,
    BuildTuple,
    BuildContext(usize),
    BuildWhereDim(usize, IFrame, Expression),
    Operator(Identifier),
    Apply(usize, usize),
    Branch(Expression, Expression, IFrame),
    Query(IFrame, usize),
    Perturb(Expression, IFrame),
    Identifier(Identifier, Domain, IFrame),
    CacheAdd(Identifier, Domain, IFrame),
}

impl StackOp {

    pub fn process(self, vm: &mut VM) -> Result<(), Box<Error>> {
        match self {
            StackOp::ProcessInput => {
                vm.process_input().unwrap();
                Ok(())
            }
            StackOp::BuildTuple => {
                // println!("[build_tuple]");
                // println!("vm.output_stack = {:?}", vm.output_stack.clone());
                // println!("vm.result_stack = {:?}", vm.result_stack.clone());
                let rhs = vm.result_stack.pop().unwrap();
                let lhs = vm.result_stack.pop().unwrap();
                match lhs.clone() {
                    Intensional::Value(lhs_value) => {
                        match rhs.clone() {
                            Intensional::Value(rhs_value) => {
                                match lhs_value {
                                    Value::Dimension(dim) => {
                                        let tuple = Tuple::new(*dim, rhs_value);
                                        let out = Intensional::Value(Value::Tuple(Box::new(tuple)));
                                        Ok(vm.result_front(out))
                                    }
                                    other => 
                                        panic!(format!("Expected dimension but found {:?}", other)),
                                }
                            }
                            Intensional::Domain(rhs_domain) => {
                                Ok(vm.result_front(Intensional::Domain(rhs_domain)))
                            }
                        }
                    }
                    Intensional::Domain(lhs_domain) => {
                        match rhs.clone() {
                            Intensional::Value(_) =>
                                Ok(vm.result_front(Intensional::Domain(lhs_domain))),
                            Intensional::Domain(rhs_domain) =>
                                Ok(vm.result_front(Intensional::Domain(lhs_domain.union(rhs_domain)))),
                        }
                    }
                }
            },
            StackOp::BuildContext(lim) => {
                // println!("[build_context]");
                // println!("vm.output_stack = {:?}", vm.output_stack.clone());
                // println!("vm.result_stack = {:?}", vm.result_stack.clone());
                let mut tuples = Context::new();
                let mut missing = Domain::new();
                for _ in 0..lim.clone() {
                    let v = vm.result_pop_front().unwrap();
                    match v {
                        Intensional::Value(value) =>
                            match value {
                                Value::Tuple(boxed_tuple) => {
                                    tuples.push_tuple(*boxed_tuple);
                                }
                                other =>
                                    panic!(format!("Expected tuple in BuildContext {:?}", other)),
                            }
                        Intensional::Domain(domain) =>
                            missing = missing.union(domain),
                    }
                }
                tuples.reverse();
                if missing.len() > 0 {
                    Ok(vm.result(Intensional::Domain(missing)))
                } else {
                    Ok(vm.result(Intensional::Value(Value::Context(tuples))))
                }
            }
            StackOp::BuildWhereDim(lim, frame, lhs) => {
                let mut values = vec![];
                let mut missing = Domain::new();

                for _ in 0..lim.clone() {
                    match vm.result_stack.pop().unwrap() {
                        Intensional::Value(value) => {
                            values.push(value);
                        }
                        Intensional::Domain(domain) => {
                            missing = missing.union(domain);
                        }
                    }
                }

                let mut xis = vec![];
                let mut dis = vec![];

                let mut context = Context::new();
                let mut domain = Domain::new();

                for i in 0..lim.clone() {
                    let di = vm.result_stack.pop().unwrap()
                        .expect_value().unwrap()
                        .expect_dimension();
                    let xi = vm.result_stack.pop().unwrap()
                        .expect_value().unwrap()
                        .expect_dimension();
                    xis.push(xi.clone());
                    dis.push(di.clone());
                    context.push(xis[i].clone(), Value::Dimension(Box::new(dis[i].clone())));
                    context.push(dis[i].clone(), values[i].clone());
                    domain.push(xis[i].clone());
                }

                if missing.len() > 0 {
                    Ok(vm.result(Intensional::Domain(missing)))
                } else {
                    let mut next_frame = frame.clone();
                    next_frame.expression = lhs;
                    next_frame.context = frame.context.perturb(context.clone());
                    next_frame.total_domain = frame.total_domain.union(domain.clone());
                    vm.input(next_frame);
                    Ok(vm.output(Either::Right(StackOp::ProcessInput)))
                }
            }
            StackOp::Operator(id) => {
                // println!("[operator]");
                // println!("vm.output_stack = {:?}", vm.output_stack.clone());
                // println!("vm.result_stack = {:?}", vm.result_stack.clone());
                let lhs = vm.result_stack.pop().unwrap()
                    .expect_value().unwrap()
                    .expect_integer();
                let rhs = vm.result_stack.pop().unwrap()
                    .expect_value().unwrap()
                    .expect_integer();
                match id.as_ref() {
                    "*" => {
                        let r = Literal::Int32(lhs * rhs);
                        Ok(vm.result(Intensional::Value(Value::Literal(r))))
                    }
                    "+" => {
                        let r = Literal::Int32(lhs + rhs);
                        Ok(vm.result(Intensional::Value(Value::Literal(r))))
                    }
                    "-" => {
                        let r = Literal::Int32(lhs - rhs);
                        Ok(vm.result(Intensional::Value(Value::Literal(r))))
                    }
                    "<=" => {
                        let r = Literal::Bool(lhs <= rhs);
                        Ok(vm.result(Intensional::Value(Value::Literal(r))))
                    }
                    other => {
                        panic!(format!("Unrecognised primitive operator {:?}", other))
                    }
                }
            }
            StackOp::Apply(pos, lim) => {
                println!("[apply]");
                // println!("vm.output_stack = {:?}", vm.output_stack.clone());
                println!("vm.result_stack = {:?}", vm.result_stack.clone());
                // println!("pos = {:?}, lim = {:?}", pos.clone(), lim.clone());
                let mut values = vec![];
                let mut missing = Domain::new();
                for i in 0..lim.clone() {
                    match vm.result_stack[pos+i].clone() {
                        Intensional::Value(value) => {
                            values.push(value)
                        }
                        Intensional::Domain(domain) => {
                            missing = missing.union(domain)
                        }
                    }
                }

                // println!("values = {:?}", values.clone());
                // println!("missing = {:?}", missing.clone());
                
                if missing.len() > 0 {
                    println!("missing len = {:?}", missing.len());
                    vm.drain_results(pos.clone() + 1, lim);
                    Ok(vm.result_i(pos, Intensional::Domain(missing)))
                } else {
                    match values[0].clone() {
                        Value::Identifier(op) => {
                            println!("applying {:?}", op.clone());
                            vm.drain_results(pos.clone() + 1, lim);
                            let mut values = values[1..].to_vec();
                            values.reverse();
                            let r = apply_operator(op, values).unwrap();
                            Ok(vm.result_i(pos, r))
                        }
                        other =>
                            panic!("Unrecognised primitive {:?} in application", other)
                    }
                }
            }
            StackOp::Branch(lhs, rhs, frame) => {
                match vm.result_stack.pop().unwrap() {
                    Intensional::Value(value) => {
                        match value {
                            Value::Literal(literal) => {
                                match literal {
                                    Literal::Bool(true) => {
                                        let mut next_frame = frame.clone();
                                        next_frame.expression = lhs;
                                        vm.input(next_frame);
                                        Ok(vm.output(Either::Right(StackOp::ProcessInput)))
                                    }
                                    Literal::Bool(false) => {
                                        let mut next_frame = frame.clone();
                                        next_frame.expression = rhs;
                                        vm.input(next_frame);
                                        Ok(vm.output(Either::Right(StackOp::ProcessInput)))
                                    }
                                    other => {
                                        panic!(format!("Expected boolean in if condition but here found {:?}", other))
                                    }
                                }
                            }
                            other => {
                                panic!("Expected literal in if condition")
                            }
                        }
                    }
                    Intensional::Domain(domain) => {
                        Ok(vm.result(Intensional::Domain(domain)))
                    }
                }
            }
            StackOp::Query(frame, i) => {
                // println!("[query]");
                // println!("vm.output_stack = {:?}", vm.output_stack.clone());
                // println!("vm.result_stack = {:?}", vm.result_stack.clone());
                // println!("frame = {:?}", frame.clone());
                // println!("i = {:?}", i.clone());
                match vm.result_stack[i].clone() {
                    Intensional::Value(value) => {
                        match value {
                            Value::Dimension(di) => {
                                let di = *di;
                                if frame.total_domain.contains(di.clone()) {
                                    let value = frame.context.lookup(di)
                                        .expect("Dimension does not exist in context");
                                    Ok(vm.result_i(i.clone(), Intensional::Value(value)))
                                } else {
                                    let mut dom = Domain::new();
                                    dom.push(di);
                                    Ok(vm.result_i(i.clone(), Intensional::Domain(dom)))
                                }
                            }
                            other => {
                                panic!("Expected dimension in query but here found {:?}", other)
                            }
                        }
                    }
                    Intensional::Domain(domain) => {
                        Ok(vm.result(Intensional::Domain(domain)))
                    }
                }
            }
            StackOp::Perturb(lhs, frame) => {
                // println!("[perturb]");
                // println!("vm.output_stack = {:?}", vm.output_stack.clone());
                // println!("vm.result_stack = {:?}", vm.result_stack.clone());
                // println!("frame = {:?}", frame.clone());
                match vm.result_stack.pop().unwrap() {
                    Intensional::Value(value) => {
                        match value {
                            Value::Context(context) => {
                                let mut next_frame = frame.clone();
                                next_frame.expression = lhs;
                                next_frame.context = frame.context.perturb(context.clone());
                                next_frame.total_domain = frame.total_domain.union(context.domain());
                                vm.input(next_frame);
                                Ok(vm.output(Either::Right(StackOp::ProcessInput)))
                            }
                            other => {
                                panic!("Expected context on rhs of perturbation")
                            }
                        }
                    },
                    Intensional::Domain(domain) => {
                        Ok(vm.result(Intensional::Domain(domain)))
                    },
                }
            }
            StackOp::Identifier(id, delta, frame) => {
                println!("[identifier]");
                // println!("vm.output_stack = {:?}", vm.output_stack.clone());
                println!("vm.result_stack = {:?}", vm.result_stack.clone());
                // println!("delta = {:?}", delta.clone());
                let context = frame.context.clone().restrict(delta.clone());
                match vm.cache.find(id.clone(), context.clone()).map(|x| x.clone()) {
                    Some(value) => {
                        Ok(vm.result(value))
                    }
                    None => {
                        let module = frame.module.clone();
                        let equation = module.environment.lookup(id.clone());
                        let mut next_frame = frame.clone();
                        next_frame.expression = equation;
                        next_frame.total_domain = delta.clone();
                        vm.input(next_frame);
                        vm.output(Either::Right(StackOp::CacheAdd(id, delta, frame)));
                        Ok(vm.output(Either::Right(StackOp::ProcessInput)))
                    }
                }
            }
            StackOp::CacheAdd(id, delta, frame) => {
                println!("[cache_add]");
                // println!("vm.output_stack = {:?}", vm.output_stack.clone());
                println!("vm.result_stack = {:?}", vm.result_stack.clone());
                let value = vm.result_stack.pop().unwrap();
                let context = frame.context.clone().restrict(delta.clone());
                vm.cache.add(id.clone(), context, value.clone());
                match value.clone() {
                    Intensional::Value(v) => {
                        Ok(vm.result(Intensional::Value(v.clone())))
                    },
                    Intensional::Domain(missing) => {
                        // If missing domain is a subset of the total domain
                        if missing.clone().is_subset(frame.total_domain.clone()) {
                            // If misssing domain is a subset of the contextual domain
                            if missing.clone().is_subset(frame.context.clone().domain()) {
                                let mut next_frame = frame.clone();
                                let delta = delta.union(missing).to_vec();
                                next_frame.expression = Expression::Xi(id.clone(), delta);
                                vm.input(next_frame);
                                Ok(vm.output(Either::Right(StackOp::ProcessInput)))
                            } else {
                                // The dimensions in the domain are missing from the context
                                Ok(vm.result(value.clone()))
                            }
                        } else {
                            // The dimensions in the domain are missing from the total domain
                            Ok(vm.result(Intensional::Domain(missing.difference(frame.total_domain.clone()))))
                        }
                    },
                }
            }
        }
    }

}

#[derive(Clone)]
pub struct IFrame {
    pub expression: Expression,
    pub context: Context,
    pub init_domain: Domain,
    pub total_domain: Domain,
    pub module: Module,
}

impl fmt::Debug for IFrame {

    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let expression_s = print_expression(self.expression.clone(), 0);
        // let context_s = self.context.restrict(self.total_domain.clone());
        // let context_s = self.context.print();
        // write!(f, "{} @ {}", expression_s, context_s)
        write!(f, "{}", expression_s)
    }
}

impl IFrame {

    pub fn new(expression: Expression, module: Module) -> IFrame {
        IFrame {
            expression: expression,
            context: Context::new(),
            init_domain: Domain::new(),
            total_domain: Domain::new(),
            module: module,
        }
    }

    pub fn process_input(&self, vm: &mut VM) -> Result<(), Box<Error>> {
        match self.expression.clone() {
            Expression::Literal(literal) => {
                let literal = Intensional::Value(Value::Literal(literal));
                // println!("[literal] {:?}", literal);
                vm.result(literal);
                Ok(())
            }
            Expression::Dimension(dimension) => {
                if self.total_domain.contains(dimension.clone()) {
                    let value = self.context.lookup(dimension.clone())
                        .expect("Expected dimension in context");
                    let value = Intensional::Value(value);
                    // println!("[dimension] Found {:?}", value.clone());
                    vm.result(value);
                    Ok(())
                } else {
                    let mut h = HashSet::new();
                    h.insert(dimension.clone());
                    let domain = Intensional::Domain(Domain(h));
                    vm.result(domain);
                    Ok(())
                }
            },
            Expression::Operator(id) => {
                // println!("[operator] Found {:?}", id.clone());
                // vm.output(Either::Right(StackOp::Operator(id)));
                vm.result(Intensional::Value(Value::Identifier(id.to_owned())));
                Ok(())
            }
            Expression::TupleExpression(tuple_exp) => {
                let mut lhs = self.clone();
                lhs.expression = tuple_exp.lhs.clone();
                let mut rhs = self.clone();
                rhs.expression = tuple_exp.rhs;
                vm.input(rhs);
                vm.input(lhs);
                Ok(())
            }
            Expression::TupleBuilder(tuple_exp_vec) => {
                let lim = tuple_exp_vec.len();
                vm.output(Either::Right(StackOp::BuildContext(lim)));
                for tuple_exp in tuple_exp_vec.into_iter() {
                    let mut frame = self.clone();
                    frame.expression = Expression::TupleExpression(Box::new(tuple_exp));
                    vm.input(frame);
                    vm.output(Either::Right(StackOp::BuildTuple));
                }
                Ok(())
            }
            Expression::If(if_exp) => {
                let mut frame = self.clone();
                frame.expression = if_exp.condition.clone();
                vm.output(Either::Right(StackOp::Branch(
                    if_exp.consequent.clone(), if_exp.alternate.clone(), self.clone()
                )));
                vm.input(frame);
                Ok(())
            }
            Expression::Application(exp_vec) => {
                let pos = vm.result_stack.len();
                vm.output(Either::Right(StackOp::Apply(pos, exp_vec.len())));
                for i in 1..exp_vec.len() {
                    let mut frame = self.clone();
                    frame.expression = exp_vec[i].clone();
                    // println!("[application] inputting frame = {:?}", frame.clone());
                    vm.input(frame);
                }
                let mut frame = self.clone();
                frame.expression = exp_vec[0].clone();
                // println!("[application] inputting frame = {:?}", frame.clone());
                vm.input(frame);
                Ok(())
            }
            Expression::Query(exp) => {
                let mut frame = self.clone();
                frame.expression = (*exp).clone();
                let lim = vm.result_stack.len();
                vm.output(Either::Right(StackOp::Query(self.clone(), lim.clone())));
                // println!("[query] inputting frame = {:?}", frame.clone());
                vm.input(frame);
                Ok(())
            }
            Expression::Perturb(perturb_exp) => {
                let mut frame = self.clone();
                frame.expression = perturb_exp.rhs.clone();
                vm.output(Either::Right(StackOp::Perturb(perturb_exp.lhs, self.clone())));
                vm.input(frame);
                Ok(())
            }
            Expression::WhereVar(wherevar_exp) => {
                let mut next_frame = self.clone();
                next_frame.expression = wherevar_exp.lhs.clone();
                next_frame.module.merge_environment(wherevar_exp.rhs.clone());
                vm.input(next_frame);
                Ok(())
            }                
            Expression::WhereDim(wheredim_exp) => {
                let wheredim_exp_vec = wheredim_exp.rhs.0.clone();
                let lim = wheredim_exp_vec.len();
                let lhs = wheredim_exp.lhs.clone();
                vm.output(Either::Right(StackOp::BuildWhereDim(lim, self.clone(), lhs)));
                for dimension_exp in wheredim_exp_vec.into_iter() {
                    let xi = dimension_exp.lhs.clone();
                    let (_, di_value) = xi.generate_unique();
                    vm.result(Intensional::Value(Value::Dimension(Box::new(xi))));
                    vm.result(Intensional::Value(di_value));
                    let mut rhs = self.clone();
                    rhs.expression = dimension_exp.rhs.clone();
                    vm.input(rhs);
                }
                Ok(())
            }
            Expression::Identifier(id) => {
                vm.output(Either::Right(StackOp::Identifier(id, Domain::new(), self.clone())));
                Ok(())
            }
            Expression::Xi(id, delta) => {
                vm.output(Either::Right(StackOp::Identifier(id, Domain::from_vec(delta), self.clone())));
                Ok(())
            }
            _ => {
                Err(From::from(format!("Unrecognised expression {:?} in input", self.expression)))
            }
        }            
    }

}
