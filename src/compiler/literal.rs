use colored::Colorize;
use std::fmt;

#[derive(Hash, Serialize, Deserialize, Eq, PartialEq, Clone)]
pub enum Literal {
    Bool(bool),
    Int32(u32),
    Hex(String),
}

impl fmt::Debug for Literal {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Literal::Bool(b) => {
                let s = format!("{:?}", b).bright_cyan();
                write!(f, "{}", s)
            },
            Literal::Int32(i) => {
                let s = format!("{:?}", i).bright_cyan();
                write!(f, "{}", s)
            },
            Literal::Hex(h) => {
                let s = format!("{:?}", h).bright_cyan();
                write!(f, "{}", s)
            }
        }
    }
}
