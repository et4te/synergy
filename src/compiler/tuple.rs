use expression::Expression;

#[derive(Hash, Serialize, Deserialize, Eq, PartialEq, Clone, Debug)]
pub struct TupleExpression {
    pub lhs: Expression,
    pub rhs: Expression,
}
