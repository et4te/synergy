use value::Value;
use domain::Domain;
use std::error::Error;
use std::fmt;

#[derive(Clone)]
pub enum Intensional {
    Value(Value),
    Domain(Domain),
}

impl fmt::Debug for Intensional {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self.clone() {
            Intensional::Value(value) =>
                write!(f, "{:?}", value),
            Intensional::Domain(domain) =>
                write!(f, "{:?}", domain),
        }
    }
}

impl Intensional {
    pub fn expect_value(&self) -> Result<Value, Box<Error>> {
        match *self {
            Intensional::Value(ref value) =>
                Ok(value.clone()),
            Intensional::Domain(ref missing) =>
                Err(From::from(format!("Expected value but here found {:?}", missing.clone()))),
        }
    }
}
