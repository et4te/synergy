use environment::Environment;
use value::{Value, Dimension};
use intensional::Intensional;
use domain::Domain;
use context::Context;
use futures::Future;
use futures::future::{ok, err, join_all};
use std::collections::HashSet;
use std::error::Error;
use std::rc::Rc;
use std::fmt;

use compiler::{Frame, Literal, TupleExpression};

type Identifier = String;

#[derive(Hash, Serialize, Deserialize, Eq, PartialEq, Clone, Debug)]
pub struct ExternalFunction {
    pub name: Identifier,
    pub parameters: Vec<Expression>,
}

#[derive(Hash, Serialize, Deserialize, Eq, PartialEq, Clone)]
pub struct BaseAbstraction {
    pub dimensions: Vec<Dimension>,
    pub body: Expression,
}

impl fmt::Debug for BaseAbstraction {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let s = format!(".\\ {:?} -> ", self.dimensions.clone());
        write!(
            f,
            "{} {}",
            s,
            super::print_expression(self.body.clone(), 0)
        )
    }
}

#[derive(Hash, Serialize, Deserialize, Eq, PartialEq, Clone, Debug)]
pub struct BaseApplication {
    pub lhs: Expression,
    pub args: Vec<Expression>,
}

#[derive(Hash,  Serialize, Deserialize,Eq, PartialEq, Clone)]
pub struct ValueAbstraction {
    // pub formal_parameters: Vec<Identifier>,
    pub dimensions: Vec<Dimension>,
    pub body: Expression,
}

impl fmt::Debug for ValueAbstraction {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let s = format!("!\\ {:?} -> ", self.dimensions.clone());
        write!(
            f,
            "{} {}",
            s,
            super::print_expression(self.body.clone(), 0)
        )
    }
}

#[derive(Hash, Serialize, Deserialize, Eq, PartialEq, Clone, Debug)]
pub struct ValueApplication {
    pub lhs: Expression,
    pub args: Vec<Expression>,
}

#[derive(Hash, Serialize, Deserialize, Eq, PartialEq, Clone)]
pub struct NameAbstraction {
    pub dimensions: Vec<Dimension>,
    pub body: Expression,
}

impl fmt::Debug for NameAbstraction {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let s = format!("\\ {:?} -> ", self.dimensions.clone());
        write!(
            f,
            "{} {}",
            s,
            super::print_expression(self.body.clone(), 0)
        )
    }
}

#[derive(Hash, Serialize, Deserialize, Eq, PartialEq, Clone, Debug)]
pub struct NameApplication {
    pub lhs: Expression,
    pub args: Vec<Expression>,
}

#[derive(Hash, Serialize, Deserialize, Eq, PartialEq, Clone, Debug)]
pub struct IntensionExpression {
    pub domain: Vec<Expression>,
    pub value: Expression,
}

#[derive(Hash, Serialize, Deserialize, Eq, PartialEq, Clone, Debug)]
pub struct IfExpression {
    pub condition: Expression,
    pub consequent: Expression,
    pub alternate: Expression,
}

#[derive(Hash, Serialize, Deserialize, Eq, PartialEq, Clone, Debug)]
pub struct WhereVarExpression {
    pub lhs: Expression,
    pub rhs: Environment,
}

#[derive(Hash, Serialize, Deserialize, Eq, PartialEq, Clone, Debug)]
pub struct PerturbExpression {
    pub lhs: Expression,
    pub rhs: Expression,
}

#[derive(Hash, Serialize, Deserialize, Eq, PartialEq, Clone, Debug)]
pub struct DimensionExpression {
    pub lhs: Dimension,
    pub rhs: Expression,
}

#[derive(Hash, Serialize, Deserialize, Eq, PartialEq, Clone, Debug)]
pub struct ContextExpression(pub Vec<DimensionExpression>);

#[derive(Hash, Serialize, Deserialize, Eq, PartialEq, Clone, Debug)]
pub struct WhereDimExpression {
    pub nat_q: u32,       // The wheredim label q E N
    pub dim_q: Dimension, // A unique dimension (since q is unique)
    pub lhs: Expression,
    pub rhs: ContextExpression,
}

#[derive(Hash, Serialize, Deserialize, Eq, PartialEq, Clone, Debug)]
pub struct FunctionApplication {
    pub id: Identifier,
    pub base_args: Vec<Expression>,
    pub value_args: Vec<Expression>,
}

#[derive(Hash, Serialize, Deserialize, Eq, PartialEq, Clone, Debug)]
pub struct DeclarationExpression {
    pub lhs: Expression,
    pub rhs: Expression,
}

#[derive(Hash, Serialize, Deserialize, Eq, PartialEq, Clone, Debug)]
pub enum Expression {
    // Top-level Scope
    DimensionDeclaration(Box<DimensionExpression>),
    // Inner Expressions
    Literal(Literal),
    Dimension(Dimension),
    Identifier(Identifier),
    Xi(Identifier, Vec<Dimension>),
    Operator(Identifier),
    ExternalFunction(Box<ExternalFunction>),
    Sequence(Vec<Expression>),
    TupleExpression(Box<TupleExpression>),
    TupleBuilder(Vec<TupleExpression>),
    BaseAbstraction(Box<BaseAbstraction>),
    BaseApplication(Box<BaseApplication>),
    ValueAbstraction(Box<ValueAbstraction>),
    ValueApplication(Box<ValueApplication>),
    FunctionApplication(Box<FunctionApplication>),
    IntensionBuilder(Box<IntensionExpression>),
    IntensionApplication(Box<Expression>),
    Application(Vec<Expression>),
    If(Box<IfExpression>),
    WhereVar(Box<WhereVarExpression>),
    Query(Box<Expression>),
    Perturb(Box<PerturbExpression>),
    WhereDim(Box<WhereDimExpression>),
}

impl Expression {
    pub fn as_identifier(&self) -> Identifier {
        match self.clone() {
            Expression::Identifier(id) => id,

            _ => panic!("Expected identifier"),
        }
    }
}
