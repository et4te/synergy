use std::collections::HashMap;
use l1::expression::*;

type Identifier = String;

#[derive(PartialEq, Clone, Debug)]
pub struct L1Environment(pub HashMap<Identifier, L1Expression>);

impl L1Environment {
    pub fn new() -> L1Environment {
        L1Environment(HashMap::new())
    }

    pub fn lookup(&self, x: Identifier) -> &L1Expression {
        match self.0.get(&x) {
            Some(xi) => xi,
            None => panic!(format!("Undefined identifier {}", x)),
        }
    }

    pub fn define(&mut self, id: Identifier, x: L1Expression) {
        self.0.insert(id, x);
    }

    pub fn merge(&mut self, other: L1Environment) {
        self.0.extend(other.0)
    }

    pub fn len(&self) -> usize {
        self.0.len()
    }
}
