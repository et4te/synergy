use l1::environment::L1Environment;
use environment::Environment;
use expression::*;
use value::{Dimension, Value};
use domain::Domain;
use compiler::{Literal, TupleExpression};
use std::collections::{HashMap, HashSet};

type Identifier = String;

pub fn params_to_ids(exprs: Vec<L1Expression>) -> Vec<Identifier> {
    let mut ids: Vec<Identifier> = vec![];
    for expr in exprs {
        let id = expr.expect_identifier();
        ids.push(id);
    }
    ids
}

#[derive(PartialEq, Clone, Debug)]
pub struct L1TupleExpression {
    pub lhs: L1Expression,
    pub rhs: L1Expression,
}

#[derive(PartialEq, Clone, Debug)]
pub struct L1BaseAbstraction {
    pub parameters: Vec<Identifier>,
    pub body: L1Expression,
}

#[derive(PartialEq, Clone, Debug)]
pub struct L1BaseApplication {
    pub lhs: L1Expression,
    pub rhs: L1Expression,
}

#[derive(PartialEq, Clone, Debug)]
pub struct L1ValueAbstraction {
    pub parameters: Vec<Identifier>,
    pub body: L1Expression,
}

#[derive(PartialEq, Clone, Debug)]
pub struct L1ValueApplication {
    pub lhs: L1Expression,
    pub rhs: L1Expression,
}

#[derive(PartialEq, Clone, Debug)]
pub struct L1NameAbstraction {
    pub parameters: Vec<Identifier>,
    pub body: L1Expression,
}

#[derive(PartialEq, Clone, Debug)]
pub struct L1NameApplication {
    pub lhs: L1Expression,
    pub rhs: L1Expression,
}

#[derive(PartialEq, Clone, Debug)]
pub struct L1IntensionExpression {
    pub domain: Vec<L1Expression>,
    pub value: L1Expression,
}

#[derive(PartialEq, Clone, Debug)]
pub struct L1IfExpression {
    pub condition: L1Expression,
    pub consequent: L1Expression,
    pub alternate: L1Expression,
}

#[derive(PartialEq, Clone, Debug)]
pub struct L1WhereVarExpression {
    pub lhs: L1Expression,
    pub rhs: L1Environment,
}

#[derive(PartialEq, Clone, Debug)]
pub struct L1PerturbExpression {
    pub lhs: L1Expression,
    pub rhs: L1Expression,
}

#[derive(PartialEq, Clone, Debug)]
pub struct L1DimensionExpression {
    pub lhs: Identifier,
    pub rhs: L1Expression,
}

#[derive(PartialEq, Clone, Debug)]
pub struct L1ContextExpression(pub Vec<L1DimensionExpression>);

#[derive(PartialEq, Clone, Debug)]
pub struct L1WhereDimExpression {
    pub lhs: L1Expression,
    pub rhs: L1ContextExpression,
}

#[derive(PartialEq, Clone, Debug)]
pub struct L1DeclarationExpression {
    pub lhs: L1Expression,
    pub tuple_builder: Option<L1Expression>,
    pub rhs: L1Expression,
}

#[derive(PartialEq, Clone, Debug)]
pub struct L1FunctionApplication {
    pub lhs: L1Expression,
    pub base_args: Vec<L1Expression>,
    pub value_args: Vec<L1Expression>,
    pub name_args: Vec<L1Expression>,
}

#[derive(PartialEq, Clone, Debug)]
pub struct L1FunctionDeclaration {
    pub name: L1Expression,
    pub base_parameters: Vec<Identifier>,
    pub value_parameters: Vec<Identifier>,
    pub name_parameters: Vec<Identifier>,
    pub body: L1Expression,
}

impl L1FunctionDeclaration {

    pub fn to_abstraction(self) -> L1Expression {
        if self.base_parameters.len() > 0 {
            if self.value_parameters.len() > 0 {
                if self.name_parameters.len() > 0 {
                    let name_abstraction = L1NameAbstraction {
                        parameters: self.name_parameters.clone(),
                        body: self.body.clone(),
                    };
                    let value_abstraction = L1ValueAbstraction {
                        parameters: self.value_parameters.clone(),
                        body: L1Expression::NameAbstraction(Box::new(name_abstraction.clone())),
                    };
                    let base_abstraction = L1BaseAbstraction {
                        parameters: self.base_parameters.clone(),
                        body: L1Expression::ValueAbstraction(Box::new(value_abstraction.clone())),
                    };
                    L1Expression::BaseAbstraction(Box::new(base_abstraction))
                } else {
                    let value_abstraction = L1ValueAbstraction {
                        parameters: self.value_parameters.clone(),
                        body: self.body.clone(),
                    };
                    let base_abstraction = L1BaseAbstraction {
                        parameters: self.base_parameters.clone(),
                        body: L1Expression::ValueAbstraction(Box::new(value_abstraction.clone())),
                    };
                    L1Expression::BaseAbstraction(Box::new(base_abstraction))
                }
            } else {
                if self.name_parameters.len() > 0 {
                    let name_abstraction = L1NameAbstraction {
                        parameters: self.name_parameters.clone(),
                        body: self.body.clone(),
                    };
                    let base_abstraction = L1BaseAbstraction {
                        parameters: self.base_parameters.clone(),
                        body: L1Expression::NameAbstraction(Box::new(name_abstraction.clone())),
                    };
                    L1Expression::BaseAbstraction(Box::new(base_abstraction))
                } else {
                    let base_abstraction = L1BaseAbstraction {
                        parameters: self.base_parameters.clone(),
                        body: self.body.clone(),
                    };
                    L1Expression::BaseAbstraction(Box::new(base_abstraction))
                }
            }
        } else {
            if self.value_parameters.len() > 0 {
                if self.name_parameters.len() > 0 {
                    let name_abstraction = L1NameAbstraction {
                        parameters: self.name_parameters.clone(),
                        body: self.body.clone(),
                    };
                    let value_abstraction = L1ValueAbstraction {
                        parameters: self.value_parameters.clone(),
                        body: L1Expression::NameAbstraction(Box::new(name_abstraction.clone())),
                    };
                    L1Expression::ValueAbstraction(Box::new(value_abstraction))
                } else {
                    let value_abstraction = L1ValueAbstraction {
                        parameters: self.value_parameters.clone(),
                        body: self.body.clone(),
                    };
                    L1Expression::ValueAbstraction(Box::new(value_abstraction))
                }
            } else {
                if self.name_parameters.len() > 0 {
                    let name_abstraction = L1NameAbstraction {
                        parameters: self.name_parameters.clone(),
                        body: self.body.clone(),
                    };
                    L1Expression::NameAbstraction(Box::new(name_abstraction))
                } else {
                    self.body.clone()
                }
            }
        }
    }
}

#[derive(PartialEq, Clone, Debug)]
pub struct L1WhereExpression {
    pub lhs: L1Expression,
    pub rhs: Vec<L1Expression>,
}

#[derive(PartialEq, Clone, Debug)]
pub enum L1Expression {
    // Transformed from L1Expression to WhereDim
    DimensionDeclaration(Box<L1DeclarationExpression>),
    // Transformed from L1Expression to WhereVar
    VariableDeclaration(Box<L1DeclarationExpression>),
    // Transformed from L1Expression to WhereVar + Abstractions
    FunctionDeclaration(Box<L1FunctionDeclaration>),
    // The rest are transformed 1:1 from L1Expression to corresponding Expression form
    Literal(Literal),
    Identifier(Identifier),
    Operator(Identifier),
    Sequence(Vec<L1Expression>),
    TupleBuilder(Vec<L1TupleExpression>),
    BaseAbstraction(Box<L1BaseAbstraction>),
    BaseApplication(Box<L1BaseApplication>),
    ValueAbstraction(Box<L1ValueAbstraction>),
    ValueApplication(Box<L1ValueApplication>),
    NameAbstraction(Box<L1NameAbstraction>),
    NameApplication(Box<L1NameApplication>),
    FunctionApplication(Box<L1FunctionApplication>),
    IntensionBuilder(Box<L1IntensionExpression>),
    IntensionApplication(Box<L1Expression>),
    Application(Vec<L1Expression>),
    If(Box<L1IfExpression>),
    WhereVar(Box<L1WhereVarExpression>),
    Query(Box<L1Expression>),
    Perturb(Box<L1PerturbExpression>),
    WhereDim(Box<L1WhereDimExpression>),
}

#[derive(Debug, Clone)]
pub struct Path(pub Vec<u32>);

impl Path {

    pub fn new() -> Path {
        Path(vec![])
    }
}

pub fn is_prefix_of(p1: &Path, p2: &Path) -> bool {
    if p1.0.len() > p2.0.len() {
        false
    } else {
        for i in 0..p1.0.len() {
            if p1.0[i] != p2.0[i] {
                return false
            }
        }
        true
    }
}

#[derive(Debug, Clone)]
pub struct PathMap {
    pub map: HashMap<Identifier, (usize, Path)>,
}

impl PathMap {

    fn new() -> PathMap {
        PathMap { map: HashMap::new() }
    }

    fn from_params(p: Path, params: Vec<Identifier>) -> PathMap {
        let mut i: usize = 0;
        let mut m: HashMap<Identifier, (usize, Path)> = HashMap::new();
        for param in params {
            m.insert(param.clone(), (i, p.clone()));
            i += 1;
        }
        PathMap { map: m }
    }

    fn insert(&mut self, id: Identifier, p: Path, i: u32, pos: usize) {
        let pi = [p.0.clone(), vec![i]].concat();
        self.map.insert(id.clone(), (pos as usize, Path(pi)));
    }
}

#[derive(Debug)]
pub struct L1State {
    // Abstraction parameters create paths in a path map which is used
    // to determine the scope of a generated dimension. For example if
    // an identifier has two paths and an identifier is found within
    // the body of a sub-expression, the common prefix is taken to be
    // the correct dimension.
    pub paths: HashMap<Identifier, Vec<Path>>,
    // Set of names in named abstractions which should be converted to
    // intension applications.
    pub names: HashMap<Identifier, Vec<Path>>,
    pub p: Path,
    pub q_index: u32,
    pub q_dimensions: HashSet<Dimension>,
}

pub fn generate_static_dimension(i: u32, id: Identifier) -> Dimension {
    Dimension {
        p: vec![i],
        v: Value::Identifier(id.clone()),
    }
}

impl L1State {

    pub fn new() -> L1State {
        L1State {
            paths: HashMap::new(),
            names: HashMap::new(),
            p: Path::new(),
            q_index: 0u32,
            q_dimensions: HashSet::new(),
        }
    }

    pub fn push_branch(&mut self) {
        self.p.0.push(0);
    }

    pub fn pop_branch(&mut self) {
        self.p.0.pop();
    }

    pub fn new_branch(&mut self, i: u32) {
        self.p.0.push(i);
    }

    pub fn lookup_name(&self, id: Identifier) -> Option<Dimension> {
        // println!("looking up name with id = {:?}", id.clone());
        match self.names.get(&id) {
            Some(v) => {
                // println!("found v = {:?} where p = {:?}", v.clone(), self.p.clone());
                let mut l = vec![];
                for p in v.iter() {
                    if is_prefix_of(p, &self.p) {
                        if p.0.len() > l.len() {
                            l = p.0.clone();
                        }
                    }
                }
                if l.len() == 0 {
                    None
                } else {
                    let di = Dimension { p: l.clone(), v: Value::Identifier(id.clone()) };
                    Some(di)
                }
            }
            None => None
        }
    }

    pub fn lookup_dimension(&self, id: Identifier) -> Option<Dimension> {
        match self.paths.get(&id) {
            Some(v) => {
                let mut l = vec![];
                for p in v.iter() {
                    if is_prefix_of(p, &self.p) {
                        if p.0.len() > l.len() {
                            l = p.0.clone();
                        }
                    }
                }
                if l.len() == 0 {
                    None
                } else {
                    let di = Dimension { p: l.clone(), v: Value::Identifier(id.clone()) };
                    Some(di)
                }
            }
            None => None
        }
    }

    pub fn generate_dimension(&self, i: u32, id: Identifier) -> Dimension {
        Dimension {
            p: [self.p.0.clone(), vec![i]].concat(),
            v: Value::Identifier(id.clone()),
        }
    }

    // Generate dimensions from wheredim <q,p> where q is used to cache re-entrant
    // dimensions.
    // pub fn generate_static_dimensions() { }

    // Generate dimensions from named parameters
    pub fn generate_names(&mut self, parameters: Vec<Identifier>) -> Vec<Dimension> {
        let path_map = PathMap::from_params(self.p.clone(), parameters.clone());
        self.merge_name_map(path_map.clone());

        let default_dim = Dimension { p: vec![], v: Value::Identifier("0".to_owned()) };
        let mut dimensions = vec![default_dim; parameters.len()];
        for (id, (i, p)) in path_map.map.into_iter() {
            let di = Dimension { p: p.0.clone(), v: Value::Identifier(id.clone()) };
            dimensions[i] = di;
        }
        dimensions
    }

    // Generate dimensions from abstraction parameters
    pub fn generate_dimensions(&mut self, parameters: Vec<Identifier>) -> Vec<Dimension> {
        let path_map = PathMap::from_params(self.p.clone(), parameters.clone());
        self.merge_path_map(path_map.clone());

        let default_dim = Dimension { p: vec![], v: Value::Identifier("0".to_owned()) };
        let mut dimensions = vec![default_dim; parameters.len()];
        for (id, (i, p)) in path_map.map.into_iter() {
            let di = Dimension { p: p.0.clone(), v: Value::Identifier(id.clone()) };
            dimensions[i] = di;
        }
        dimensions
    }

    pub fn merge_name_map(&mut self, path_map: PathMap) {
        for (id, (_, p)) in path_map.map.into_iter() {
            self.names.entry(id)
                .and_modify(|e| { (*e).push(p.clone()) })
                .or_insert(vec![p.clone()]);
        }
    }

    pub fn merge_path_map(&mut self, path_map: PathMap) {
        for (id, (_, p)) in path_map.map.into_iter() {
            self.paths.entry(id)
                .and_modify(|e| { (*e).push(p.clone()) })
                .or_insert(vec![p.clone()]);
        }
    }

    pub fn domain(&self) -> Domain {
        let mut dom = Domain::new();
        for (id, path_v) in self.paths.clone() {
            for p in path_v {
                let di = Dimension { p: p.0, v: Value::Identifier(id.clone()) };
                dom.push(di);
            }
        }
        for (id, path_v) in self.names.clone() {
            for p in path_v {
                let di = Dimension { p: p.0, v: Value::Identifier(id.clone()) };
                dom.push(di);
            }
        }
        dom
    }
}

impl L1Expression {
    pub fn transform(self, state: &mut L1State) -> Expression {
        match self {
            L1Expression::Literal(literal) =>
                Expression::Literal(literal),
            L1Expression::Operator(id) =>
                Expression::Operator(id),
            L1Expression::Sequence(expr_vec) => {
                let mut seq = vec![];
                for expr in expr_vec {
                    let r = expr.transform(state);
                    seq.push(r);
                }
                Expression::Sequence(seq)
            }
            L1Expression::TupleBuilder(tuple_expr) => {
                let mut tuple_exprs = vec![];
                for tuple in tuple_expr {
                    let lhs_r = tuple.lhs.transform(state);
                    let rhs_r = tuple.rhs.transform(state);
                    tuple_exprs.push(
                        TupleExpression {
                            lhs: lhs_r,
                            rhs: rhs_r,
                        }
                    )
                }
                Expression::TupleBuilder(tuple_exprs)
            }
            L1Expression::Application(application_expr) => {
                let mut expr_vec = vec![];
                let op = application_expr[0].clone();
                let op = op.transform(state);
                for i in 1..application_expr.len() {
                    let arg = application_expr[i].clone();
                    let arg = arg.transform(state);
                    expr_vec.push(arg);
                }
                Expression::Application([vec![op], expr_vec].concat())
            }
            L1Expression::If(if_expr) => {
                state.push_branch();
                let condition = if_expr.condition.clone();
                let condition = condition.transform(state);
                state.new_branch(1);
                let consequent = if_expr.consequent.clone();
                let consequent = consequent.transform(state);
                state.pop_branch();
                state.new_branch(2);
                let alternate = if_expr.alternate.clone();
                let alternate = alternate.transform(state);
                state.pop_branch();
                state.pop_branch();
                let if_expr = IfExpression {
                    condition: condition,
                    consequent: consequent,
                    alternate: alternate,
                };
                Expression::If(Box::new(if_expr))
            }
            L1Expression::WhereVar(wv) => {
                let mut env = Environment::new();
                for (id, expr) in wv.rhs.0.clone() {
                    let expr = expr.transform(state);
                    env.define(id, expr);
                }
                let lhs = wv.lhs.clone();
                let lhs = lhs.transform(state);
                let wv = WhereVarExpression {
                    lhs: lhs,
                    rhs: env.clone(),
                };
                Expression::WhereVar(Box::new(wv))
            }
            L1Expression::Query(expr) => {
                let expr = *expr.clone();
                let expr = expr.transform(state);
                Expression::Query(Box::new(expr))
            }
            L1Expression::Perturb(perturb_expr) => {
                let lhs = perturb_expr.clone().lhs;
                let lhs = lhs.transform(state);
                let rhs = perturb_expr.rhs;
                let rhs = rhs.transform(state);
                let perturb_expr = PerturbExpression {
                    lhs: lhs,
                    rhs: rhs,
                };
                Expression::Perturb(Box::new(perturb_expr))
            }
            L1Expression::FunctionApplication(fapp) => {
                let lhs = fapp.lhs.clone();
                let lhs = lhs.transform(state);
                let mut base_args = vec![];
                for i in 0..fapp.base_args.len() {
                    let base_arg = fapp.base_args[i].clone();
                    let base_arg = base_arg.transform(state);
                    base_args.push(base_arg);
                }
                let mut value_args = vec![];
                for i in 0..fapp.value_args.len() {
                    let value_arg = fapp.value_args[i].clone();
                    let value_arg = value_arg.transform(state);
                    value_args.push(value_arg);
                }
                for i in 0..fapp.name_args.len() {
                    let value_arg = fapp.name_args[i].clone();
                    let value_arg = value_arg.transform(state);
                    let intension = IntensionExpression {
                        domain: vec![],
                        value: value_arg,
                    };
                    let value_arg = Expression::IntensionBuilder(Box::new(intension));
                    value_args.push(value_arg);
                }
                let fapp = FunctionApplication {
                    id: lhs.as_identifier(),
                    base_args: base_args,
                    value_args: value_args,
                };
                Expression::FunctionApplication(Box::new(fapp))
            }
            L1Expression::BaseAbstraction(babstr) => {
                state.push_branch();
                let base_dims = state.generate_dimensions(babstr.parameters.clone());
                match babstr.body {
                    L1Expression::ValueAbstraction(vabstr) => {
                        let value_dims = state.generate_dimensions(vabstr.parameters.clone());
                        match vabstr.body {
                            L1Expression::NameAbstraction(nabstr) => {
                                let name_dims = state.generate_names(nabstr.parameters.clone());
                                let body = nabstr.body.transform(state);
                                let babstr = BaseAbstraction {
                                    dimensions: base_dims,
                                    body: Expression::ValueAbstraction(Box::new(ValueAbstraction {
                                        dimensions: [value_dims, name_dims].concat(),
                                        body: body,
                                    }))
                                };
                                state.pop_branch();
                                Expression::BaseAbstraction(Box::new(babstr))
                            }
                            _ => {
                                let body = vabstr.body.transform(state);
                                let babstr = BaseAbstraction {
                                    dimensions: base_dims,
                                    body: Expression::ValueAbstraction(Box::new(ValueAbstraction {
                                        dimensions: value_dims,
                                        body: body,
                                    }))
                                };
                                state.pop_branch();
                                Expression::BaseAbstraction(Box::new(babstr))
                            }
                        }
                    }
                    L1Expression::NameAbstraction(nabstr) => {
                        let name_dims = state.generate_names(nabstr.parameters.clone());
                        let body = nabstr.body.transform(state);
                        let babstr = BaseAbstraction {
                            dimensions: base_dims,
                            body: Expression::ValueAbstraction(Box::new(ValueAbstraction {
                                dimensions: name_dims,
                                body: body,
                            })),
                        };
                        state.pop_branch();                        
                        Expression::BaseAbstraction(Box::new(babstr))
                    }
                    _ => {
                        panic!("Expected abstraction but found a different expression.")
                    }
                }
            }
            L1Expression::BaseApplication(bapp) => {
                let lhs = bapp.lhs.clone();
                let lhs = lhs.transform(state);
                let mut base_application = BaseApplication {
                    lhs: lhs,
                    args: vec![],
                };
                loop {
                    match bapp.rhs.clone() {
                        L1Expression::BaseApplication(bapp) => {
                            let arg = bapp.rhs.transform(state);
                            base_application.args.push(arg);
                            continue;
                        }
                        final_arg => {
                            let arg = final_arg.transform(state);
                            base_application.args.push(arg);
                            break;
                        }
                    }
                }
                Expression::BaseApplication(Box::new(base_application))
            }
            L1Expression::ValueAbstraction(vabstr) => {
                state.push_branch();
                let value_dims = state.generate_dimensions(vabstr.parameters.clone());
                match vabstr.body {
                    L1Expression::NameAbstraction(nabstr) => {
                        let name_dims = state.generate_names(nabstr.parameters.clone());
                        let body = nabstr.body.transform(state);
                        let vabstr = ValueAbstraction {
                            dimensions: [value_dims, name_dims].concat(),
                            body: body,
                        };
                        state.pop_branch();                        
                        Expression::ValueAbstraction(Box::new(vabstr))
                    }
                    _ => {
                        let body = vabstr.body.transform(state);
                        let vabstr = ValueAbstraction {
                            dimensions: value_dims,
                            body: body,
                        };
                        state.pop_branch();                        
                        Expression::ValueAbstraction(Box::new(vabstr))
                    }
                }
            }
            L1Expression::ValueApplication(vapp) => {
                let lhs = vapp.lhs.clone();
                let lhs = lhs.transform(state);
                let mut value_application = ValueApplication {
                    lhs: lhs,
                    args: vec![],
                };
                loop {
                    match vapp.rhs.clone() {
                        L1Expression::ValueApplication(vapp) => {
                            let arg = vapp.rhs.transform(state);
                            value_application.args.push(arg);
                            continue;
                        }
                        final_arg => {
                            let arg = final_arg.transform(state);
                            value_application.args.push(arg);
                            break;
                        }
                    }
                }
                Expression::ValueApplication(Box::new(value_application))
            }
            L1Expression::NameAbstraction(nabstr) => {
                state.push_branch();
                let name_dims = state.generate_names(nabstr.parameters.clone());
                let body = nabstr.body.transform(state);
                let value_abstr = ValueAbstraction {
                    dimensions: name_dims.clone(),
                    body: body,
                };
                state.pop_branch();
                Expression::ValueAbstraction(Box::new(value_abstr))
            }
            L1Expression::NameApplication(napp) => {
                let lhs = napp.lhs.clone();
                let lhs = lhs.transform(state);
                let mut value_application = ValueApplication {
                    lhs: lhs,
                    args: vec![],
                };
                loop {
                    match napp.rhs.clone() {
                        L1Expression::NameApplication(napp) => {
                            let arg = napp.rhs.transform(state);
                            let intension = IntensionExpression {
                                domain: vec![],
                                value: arg,
                            };
                            let expr = Expression::IntensionBuilder(Box::new(intension));
                            value_application.args.push(expr);
                            continue;
                        }
                        final_arg => {
                            let arg = final_arg.transform(state);
                            value_application.args.push(arg);
                            break;
                        }
                    }
                }
                Expression::ValueApplication(Box::new(value_application))
            }
            L1Expression::IntensionBuilder(intens_expr) => {
                let mut domain = vec![];
                for dim in intens_expr.domain.clone() {
                    let dim = dim.transform(state);
                    domain.push(dim);
                }
                let e0 = intens_expr.value.transform(state);
                let intens_expr = IntensionExpression {
                    domain: domain.clone(),
                    value: e0,
                };
                Expression::IntensionBuilder(Box::new(intens_expr))
            }
            L1Expression::IntensionApplication(intens_app) => {
                let iapp = (*intens_app).transform(state);
                Expression::IntensionApplication(Box::new(iapp))
            }
            L1Expression::Identifier(id) => {
                // Lookup dimension on path p with id
                match state.lookup_dimension(id.clone()) {
                    Some(di) => Expression::Dimension(di.clone()),
                    None => {
                        match state.lookup_name(id.clone()) {
                            Some(di) => {
                                Expression::IntensionApplication(
                                    Box::new(
                                        Expression::Dimension(di.clone())
                                    )
                                )
                            }
                            None => Expression::Identifier(id)
                        }
                    }
                }
            }
            L1Expression::WhereDim(decl_expr) => {
                let rhs = decl_expr.rhs.clone();
                let lhs = decl_expr.lhs;

                let mut path_map = PathMap::new();
                let mut dim_exprs = vec![];
                let mut i: u32 = 0;
                for dim_expr in rhs.clone().0 {
                    // Insert dimension identifier into list of known dimensions
                    let id = dim_expr.lhs.clone();
                    let di = state.generate_dimension(0u32, id.clone());
                    path_map.insert(id.clone(), state.p.clone(), 0u32, i.clone() as usize);
                    i += 1;
                    // Transform expressions according to updated dimensions
                    let expr = dim_expr.rhs.clone();
                    let expr = expr.transform(state);
                    let dim_expr = DimensionExpression {
                        lhs: di,
                        rhs: expr,
                    };
                    dim_exprs.push(dim_expr);
                }
                state.merge_path_map(path_map);

                state.push_branch();
                let lhs = lhs.transform(state);
                state.pop_branch();
                
                let dim_q = generate_static_dimension(state.q_index, "φ".to_string());
                state.q_dimensions.insert(dim_q.clone());
                let wd = WhereDimExpression {
                    nat_q: state.q_index,
                    dim_q: dim_q,
                    lhs: lhs,
                    rhs: ContextExpression(dim_exprs),
                };

                Expression::WhereDim(Box::new(wd))
            }
            L1Expression::DimensionDeclaration(dim_decl) => {
                let lhs = dim_decl.lhs.expect_identifier();
                let rhs = dim_decl.rhs.transform(state);
                let dim_expr = DimensionExpression {
                    lhs: state.generate_dimension(state.q_index, lhs),
                    rhs: rhs,
                };
                Expression::DimensionDeclaration(Box::new(dim_expr))
            }

            // Transform the function declaration into a base abstraction,
            // perhaps containing value and name abstractions.

            L1Expression::FunctionDeclaration(fun_decl) => {
                let l1_abstr = (*fun_decl).clone().to_abstraction();
                l1_abstr.transform(state)
            }

            other => panic!("Unrecognised expression {:?}", other)
        }
    }


    pub fn expect_identifier(&self) -> Identifier {
        match self.clone() {
            L1Expression::Identifier(id) => id,

            _ => panic!("Expected identifier"),
        }
    }
}
