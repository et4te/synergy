use colored::*;
use expression::*;
use context::*;
use domain::Domain;
use compiler::{Frame, Literal};
use intensional::Intensional;
use blake2b_simd::Params;
use bincode;
use futures::Future;
use futures::future::ok;
use std::collections::HashSet;
use std::error::Error;
use std::fmt;

type Identifier = String;

#[derive(Hash, Serialize, Deserialize, Eq, PartialEq, Clone)]
pub struct Dimension {
    pub p: Vec<u32>,
    pub v: Value,
}

impl fmt::Debug for Dimension {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let s = format!("{:?}", self.v.clone());
        let p = format!("{:?}", self.p).bright_white();
        write!(f, "({}:{})", p, s)
    }
}

impl Dimension {

    pub fn generate_unique(&self) -> (Dimension, Value) {
        let bytes = bincode::serialize(self).unwrap();
        let hash = Params::new()
            .hash_length(5 as usize)
            .key("unique_dimension".as_bytes())
            .to_state()
            .update(&bytes)
            .finalize();
        let id = (&hash).to_hex().to_string();
        let di = Dimension { p: self.p.clone(), v: Value::Identifier(id) };
        (di.clone(), Value::Dimension(Box::new(di)))
    }

    pub fn compile(&self, frame: Frame) -> impl Future<Item = (Intensional, Frame), Error = Box<Error>> {
        if frame.total_domain.contains(self.clone()) {
            let value = frame.context.lookup(self.clone())
                .expect("Expected dimension in context");
            ok((Intensional::Value(value), frame))
        } else {
            let mut h = HashSet::new();
            h.insert(self.clone());
            ok((Intensional::Domain(Domain(h)), frame))
        }
    }
}

#[derive(Hash, Serialize, Deserialize, Eq, PartialEq, Clone, Debug)]
pub struct Intension {
    pub k: Context,
    pub d: Vec<Dimension>,
    pub x: Box<Expression>,
}

#[derive(Hash, Serialize, Deserialize, Eq, PartialEq, Clone)]
pub enum Value {
    Literal(Literal),
    Dimension(Box<Dimension>),
    Intension(Box<Intension>),
    BaseAbstraction(Box<BaseAbstraction>),
    ValueAbstraction(Box<ValueAbstraction>),
    NameAbstraction(Box<NameAbstraction>),
    Identifier(String),
    Tuple(Box<Tuple>),
    Context(Context),
    PrimOp(Identifier),
}

impl Value {
    pub fn expect_integer(&self) -> u32 {
        match self {
            &Value::Literal(Literal::Int32(n)) => n,
            other => panic!("Expected u32 but here found {:?}", other),
        }
    }

    pub fn expect_dimension(&self) -> Dimension {
        match self {
            &Value::Dimension(ref di) => *di.clone(),

            other => panic!("Expected dimension but here found {:?}", other),
        }
    }

    pub fn expect_base_abstraction(&self) -> BaseAbstraction {
        match self {
            &Value::BaseAbstraction(ref base_abstraction) => *base_abstraction.clone(),

            _ => panic!("Expected base_abstraction."),
        }
    }

    pub fn expect_intension(&self) -> Intension {
        match self {
            &Value::Intension(ref intens) => *intens.clone(),

            _ => panic!("Expected intension."),
        }
    }
}

impl fmt::Debug for Value {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self.clone() {
            Value::Literal(lit) =>
                write!(f, "{:?}", lit),

            Value::Dimension(di) =>
                write!(f, "{:?}", *di),

            Value::Identifier(id) =>
                write!(f, "{}", id.bright_yellow()),

            Value::BaseAbstraction(base_abstraction) =>
                write!(f, "{:?}", *base_abstraction),

            Value::ValueAbstraction(value_abstraction) =>
                write!(f, "{:?}", *value_abstraction),

            Value::NameAbstraction(name_abstraction) =>
                write!(f, "{:?}", *name_abstraction),

            Value::Intension(intens) => {
                let mut s = format!("{:?}", intens.clone().k.clone().domain());
                s = format!("{} {}", s, super::print_expression(*intens.clone().x, 0));
                write!(f, "{} {}", s, intens.k.clone().print())
            }

            Value::Tuple(tuple) =>
                write!(f, "{:?}", tuple),

            Value::Context(k) =>
                write!(f, "{:?}", k),

            Value::PrimOp(op) =>
                write!(f, "{}", op.bright_white()),
        }
    }
}
