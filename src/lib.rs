#[macro_use]
extern crate serde_derive;
extern crate colored;
extern crate ed25519_dalek;
extern crate blake2b_simd;
extern crate bincode;
extern crate futures;

use colored::*;

pub mod l1;
pub mod compiler;
pub mod evaluator;
pub mod expression;
pub mod environment;
pub mod cache;
pub mod context;
pub mod domain;
pub mod value;
pub mod either;
pub mod intensional;

use expression::*;
use l1::expression::*;
use compiler::literal::Literal;
use environment::*;
use cache::Cache;
use context::Context;
use value::Value;
use domain::Domain;
use either::Either;

pub fn evaluate(expr: L1Expression) -> Either<Value, Domain> {
    let mut c = Cache::new();
    let mut e = Environment::new();
    let mut ee = ExternalEnvironment::new();

    //------------------------------------------------------------------
    // Language primitives
    //------------------------------------------------------------------

    // Context Indexing (#.d)
    e.define("#".to_string(), Expression::Operator("#".to_string()));

    // Context navigation (X @ [t <- 0])
    e.define("@".to_string(), Expression::Operator("@".to_string()));

    // Equality operators
    e.define("==".to_string(), Expression::Operator("==".to_string()));
    e.define("/=".to_string(), Expression::Operator("%".to_string()));
    e.define("<".to_string(), Expression::Operator(">=".to_string()));
    e.define("<=".to_string(), Expression::Operator("<=".to_string()));
    e.define(">".to_string(), Expression::Operator(">".to_string()));
    e.define(">=".to_string(), Expression::Operator(">=".to_string()));
    e.define("∧".to_string(), Expression::Operator("∧".to_string()));

    // Math operators
    e.define("%".to_string(), Expression::Operator("%".to_string()));
    e.define("^".to_string(), Expression::Operator("^".to_string()));
    e.define("/".to_string(), Expression::Operator("/".to_string()));
    e.define("*".to_string(), Expression::Operator("*".to_string()));
    e.define("+".to_string(), Expression::Operator("+".to_string()));
    e.define("-".to_string(), Expression::Operator("-".to_string()));

    // Cryptographic functions
    let h = Expression::Identifier("hash".to_owned());
    e.define("blake2b160".to_string(),
             Expression::ExternalFunction(
                 Box::new(
                     ExternalFunction {
                         name: "blake2b160".to_owned(),
                         parameters: vec![h],
                     }
                 )
             )
    );
    let blake2b160 = |_v| {
        // assert that parameters are correct
        Value::Literal(Literal::Int32(0u32))
    };
    ee.define("blake2b160".to_string(), Box::new(blake2b160));

    let mut k = Context::new();
    let mut l1_state = L1State::new();
    let l1_res = expr.transform(&mut l1_state);
    let mut d = Domain::new();
    
    for q_dim in l1_state.q_dimensions.clone() {
        k.push(q_dim.clone(), Value::Literal(Literal::Int32(0)));
        d.push(q_dim);
    }
    
    // println!("K :: {}", k.clone().print());
    // println!("D :: {}", d.clone().print());

    evaluator::evaluate(l1_res, &mut e, &ee, k.clone(), d.clone(), d.clone(), &mut c)
}

pub fn print_expression(expr: Expression, indent: u32) -> String {
    match expr {
        Expression::Literal(lit) => match lit {
            Literal::Bool(b) => {
                let s = format!("{:?}", b).bright_cyan();
                format!("{}", s)
            },
            Literal::Hex(h) => {
                let s = format!("0x{}", h).bright_cyan();
                format!("{}", s)
            },
            Literal::Int32(i) => {
                let s = format!("{:?}", i).bright_cyan();
                format!("{}", s)
            },
        },

        Expression::DimensionDeclaration(dim_decl) => {
            let di = dim_decl.lhs.clone();
            let v = print_expression(dim_decl.rhs.clone(), indent);
            format!("dim {:?} <- {}", di, v)
        }

        Expression::Dimension(di) => {
            format!("{:?}", di)
        }

        Expression::Operator(op) => format!("{}", op.bright_white()),

        Expression::ExternalFunction(ext_fn) =>
            format!("(external: {})", ext_fn.name.clone()),

        Expression::Sequence(exprs) => {
            let mut s = "".to_string();
            for expr in exprs {
                s = format!("{} {};", s, print_expression(expr, indent))
            }
            s
        }

        Expression::TupleBuilder(tuple_expr) => {
            let mut s = "[".bright_white().to_string();
            for i in 0..tuple_expr.len() {
                if i == (tuple_expr.len() - 1) {
                    let lhs = print_expression(tuple_expr[i].clone().lhs, indent);
                    let rhs = print_expression(tuple_expr[i].clone().rhs, indent);
                    s = format!("{}{} {} {}", s, lhs, "<-".bright_white(), rhs);
                } else {
                    let lhs = print_expression(tuple_expr[i].clone().lhs, indent);
                    let rhs = print_expression(tuple_expr[i].clone().rhs, indent);
                    s = format!("{}{} <- {}, ", s, lhs, rhs);
                }
            }
            format!("{}{}", s, "]".bright_white())
        }

        Expression::Application(app_expr) => {
            let op = print_expression(app_expr[0].clone(), indent);
            let mut args = "".to_string();
            for i in 1..app_expr.len() {
                if i == 1 {
                    args = format!("{}", print_expression(app_expr[i].clone(), indent));
                } else {
                    args = format!("{} {}", args, print_expression(app_expr[i].clone(), indent));
                }
            }
            format!("({} {})", op.yellow(), args)
        }

        Expression::If(if_expr) => {
            let condition = if_expr.condition.clone();
            let consequent = if_expr.consequent.clone();
            let alternate = if_expr.alternate.clone();
            format!(
                "\n{}{} {} {} {}{} \n{}{} {}{} \n{}{}",
                print_spaces(indent + 2),
                "if".bright_cyan(),
                print_expression(condition, indent),
                "then\n".bright_cyan(),
                print_spaces(indent + 4),
                print_expression(consequent, indent),
                print_spaces(indent + 2),
                "else\n".bright_cyan(),
                print_spaces(indent + 4),
                print_expression(alternate, indent),
                print_spaces(indent + 2),
                "end".bright_cyan()
            )
        }

        Expression::WhereVar(wv) => {
            let indent_s = print_spaces(indent);
            let rhs = wv.rhs.clone();
            let lhs = wv.lhs.clone();
            let lhs = print_expression(lhs, indent);
            let mut s = format!(
                "{}{}\n{}{}\n",
                indent_s,
                lhs,
                indent_s,
                "wherevar".bright_white()
            );
            for def in rhs.0 {
                s = format!(
                    "{}{}{} = {}\n",
                    s,
                    print_spaces(indent + 2),
                    def.id.bright_yellow(),
                    print_expression(def.equation, indent + 2)
                );
            }
            s
        }

        Expression::Query(e0) => {
            let e0 = Box::into_raw(e0);
            let e0 = unsafe { (*e0).clone() };
            format!("#.{}", print_expression(e0, indent))
        }

        Expression::Perturb(perturb_expr) => {
            let lhs = print_expression(perturb_expr.clone().lhs, indent);
            let rhs = print_expression(perturb_expr.rhs, indent);
            format!("{} {} {}", lhs, "@".bright_white(), rhs)
        }

        Expression::FunctionApplication(function_application) => {
            let op = print_expression(Expression::Identifier(function_application.id.clone()), indent);
            let mut base_args = "".to_string();
            for i in 0..function_application.base_args.len() {
                if i == 0 {
                    base_args = format!(
                        "{}",
                        print_expression(function_application.base_args[i].clone(), indent)
                    );
                } else {
                    base_args = format!(
                        "{} {}",
                        base_args,
                        print_expression(function_application.base_args[i].clone(), indent)
                    );
                }
            }
            let mut value_args = "".to_string();
            for i in 0..function_application.value_args.len() {
                if i == 0 {
                    value_args = format!(
                        "{}",
                        print_expression(function_application.value_args[i].clone(), indent)
                    );
                } else {
                    value_args = format!(
                        "{} {}",
                        value_args,
                        print_expression(function_application.value_args[i].clone(), indent)
                    );
                }
            }
            let args = format!("[{}] {}", base_args, value_args);
            format!("({} {})", op.yellow(), args)
        }

        Expression::BaseAbstraction(base_abstraction) => {
            let mut s = "(λb ".bright_white().to_string();
            for param in base_abstraction.dimensions.clone() {
                s = format!("{}{:?} ->", s, param);
            }
            format!(
                "{} {})",
                s,
                print_expression(base_abstraction.body.clone(), indent)
            )
        }

        Expression::BaseApplication(base_application) => {
            let op = print_expression(base_application.lhs.clone(), indent);
            let mut args = "".to_string();
            for i in 0..base_application.args.len() {
                if i == 0 {
                    args = format!(
                        "{}",
                        print_expression(base_application.args[i].clone(), indent)
                    );
                } else {
                    args = format!(
                        "{} {}",
                        args,
                        print_expression(base_application.args[i].clone(), indent)
                    );
                }
            }
            format!("({} {})", op.yellow(), args)
        }

        Expression::ValueAbstraction(value_abstraction) => {
            let mut s = "(λv ".bright_white().to_string();
            for param in value_abstraction.dimensions.clone() {
                s = format!("{}{:?} ->", s, param);
            }
            format!(
                "{} {})",
                s,
                print_expression(value_abstraction.body.clone(), indent)
            )
        }

        Expression::ValueApplication(value_application) => {
            let op = print_expression(value_application.lhs.clone(), indent);
            let mut args = "".to_string();
            for i in 0..value_application.args.len() {
                if i == 0 {
                    args = format!(
                        "{}",
                        print_expression(value_application.args[i].clone(), indent)
                    );
                } else {
                    args = format!(
                        "{} {}",
                        args,
                        print_expression(value_application.args[i].clone(), indent)
                    );
                }
            }
            format!("({} {})", op.yellow(), args)
        }

        Expression::IntensionBuilder(intens_expr) => {
            let mut s = "{".bright_white().to_string();
            if intens_expr.domain.len() > 0 {
                for i in 0..intens_expr.domain.len() {
                    if i == (intens_expr.domain.len() - 1) {
                        let ei = print_expression(intens_expr.domain[i].clone(), indent);
                        s = format!("{}{}", s, ei);
                    } else {
                        let ei = print_expression(intens_expr.domain[i].clone(), indent);
                        s = format!("{}{}", s, ei);
                    }
                }
            } else {
                return print_expression(intens_expr.value.clone(), indent);
            }
            s = format!("{}{}", s, "}".bright_white());
            format!(
                "{} {}",
                s,
                print_expression(intens_expr.value.clone(), indent)
            )
        }

        Expression::IntensionApplication(intens_app) => {
            format!("=> {}", print_expression((*intens_app).clone(), indent))
        }

        Expression::Identifier(id) => format!("{}", id.bright_yellow()),

        Expression::WhereDim(wd) => {
            let indent_s = print_spaces(indent);
            let rhs = wd.rhs.clone();
            let lhs = wd.lhs.clone();
            let lhs = print_expression(lhs, indent);
            let nat_q_s = format!("{}", wd.nat_q.clone());
            let dim_q_s = wd.dim_q.clone();
            let mut s = format!(
                "{}\n{}{} <{},{:?}>\n",
                lhs,
                indent_s,
                "wheredim".bright_white(),
                nat_q_s.bright_white(),
                dim_q_s
            );
            for tup in rhs.0 {
                s = format!(
                    "{}{}{:?} {} {}\n",
                    s,
                    print_spaces(indent + 2),
                    tup.lhs,
                    "<-".bright_white(),
                    print_expression(tup.rhs, indent)
                );
            }
            s = format!("{}{}{}", s, indent_s, "end".bright_white());
            s
        }
        other => panic!(format!("Unexpected expression {:?}", other.clone()))
    }
}

pub fn print_spaces(lim: u32) -> String {
    let mut s = "".to_string();
    for _ in 0..lim {
        s = format!("{} ", s);
    }
    s
}
