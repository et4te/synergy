use expression::Expression;
use value::Value;

type Identifier = String;

#[derive(Hash, Serialize, Deserialize, Eq, PartialEq, Clone, Debug)]
pub struct Definition {
    pub id: Identifier,
    pub equation: Expression,
}

#[derive(Hash, Serialize, Deserialize, Eq, PartialEq, Clone, Debug)]
pub struct Environment(pub Vec<Definition>);

impl Environment {
    pub fn new() -> Environment {
        Environment(vec![])
    }

    pub fn lookup(&self, id: Identifier) -> Expression {
        for x in self.0.clone() {
            if x.id == id {
                return x.equation.clone();
            }
        }
        panic!("Undefined identifier {}", id)
    }

    pub fn define(&mut self, id: Identifier, x: Expression) {
        self.0.push(Definition {
            id: id,
            equation: x,
        })
    }

    pub fn merge(&mut self, other: Environment) {
        self.0.extend(other.0)
    }
}

pub struct ExternalDefinition {
    pub id: Identifier,
    pub function: Box<Fn(Vec<Value>) -> Value>,
}

pub struct ExternalEnvironment(pub Vec<ExternalDefinition>);

impl ExternalEnvironment {
    pub fn new() -> ExternalEnvironment {
        ExternalEnvironment(vec![])
    }

    pub fn lookup(&self, id: Identifier) -> &ExternalDefinition {
        for x in &self.0 {
            if x.id == id {
                return x;
            }
        }
        panic!("Undefined identifier {}", id)
    }

    pub fn define(&mut self, id: Identifier, f: Box<Fn(Vec<Value>) -> Value>) {
        self.0.push(ExternalDefinition {
            id: id,
            function: f,
        })
    }

    pub fn merge(&mut self, other: ExternalEnvironment) {
        self.0.extend(other.0)
    }
}

