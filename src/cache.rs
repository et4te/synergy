use std::collections::HashMap;
use context::Context;
use domain::Domain;
use either::Either;
use intensional::Intensional;
use value::Value;

type Identifier = String;

#[derive(Hash, Eq, PartialEq, Clone, Debug)]
pub struct Key {
    pub x: Identifier,
    pub k: Context,
}

#[derive(Clone, Debug)]
pub struct ICache {
    pub cache: HashMap<Key, Intensional>
}

impl ICache {
    pub fn new() -> ICache {
        ICache {
            cache: HashMap::new(),
        }
    }

    pub fn find(&self, x: Identifier, k: Context) -> Option<&Intensional> {
        self.cache.get(&Key { x: x.clone(), k: k })
    }

    pub fn add(
        &mut self,
        x: Identifier,
        k: Context,
        v: Intensional,
    ) -> Intensional {
        match v.clone() {
            Intensional::Value(_value) => {
                println!("Inserting {} {:?} = {:?}", x.clone(), k, _value.clone());
                self.cache.insert(Key { x: x, k: k }, v.clone());
                v
            }
            Intensional::Domain(_dom) => {
                println!("Inserting {} {:?} = {:?}", x.clone(), k, _dom);
                self.cache.insert(Key { x: x, k: k }, v.clone());
                v
            }
        }
    }
}

//------------------------------------------------------------------------------
// TODO : Deprecation of the code below
//------------------------------------------------------------------------------

#[derive(Clone, Debug)]
pub struct Cache {
    pub cache: HashMap<Key, Either<Value, Domain>>,
}

impl Cache {
    pub fn new() -> Cache {
        Cache {
            cache: HashMap::new(),
        }
    }

    pub fn find(&mut self, x: Identifier, k: Context) -> Option<&Either<Value, Domain>> {
        self.cache.get(&Key { x: x.clone(), k: k })
    }

    pub fn add(
        &mut self,
        x: Identifier,
        k: Context,
        v: Either<Value, Domain>,
    ) -> Either<Value, Domain> {
        match v.clone() {
            Either::Left(_l) => {
                // println!("Inserting {} {} = {}", x.clone(), k.print(), print_value(l.clone()));
                self.cache.insert(Key { x: x, k: k }, v.clone());
                v
            }
            Either::Right(_r) => {
                // println!("Inserting {} {} = {}", x.clone(), k.print(), r.print());
                self.cache.insert(Key { x: x, k: k }, v.clone());
                v
            }
        }
    }
}
